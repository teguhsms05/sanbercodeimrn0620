import React, { Component } from 'react'
import { 
  View, 
  StyleSheet,
  Image,
  Text,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'




export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>  
        <View style={styles.about}>
            <Text style={styles.aboutText}>ABOUT ME</Text>
            <Icon style={styles.iconPerson} name="person" size={180} />
            <View style={styles.aboutCircle}></View>
            <Text style={styles.aboutName}>TEGUH SAPUTRA</Text>
            <Text style={styles.aboutJob}>React Native Developer</Text>
        </View>
        <View style={styles.body}>
            <View style={styles.portoBox}>
                <Text style={styles.textBoxPorto}>Portofolio</Text>
                <Ionicons style={styles.iconPorto}name="logo-github" size={50}></Ionicons>
                <Ionicons style={styles.iconPorto}name="logo-github" size={50}></Ionicons>
            </View>
        </View>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D9EDEC',
  },
  about: {
      flex: 1,
      alignItems: 'center'
  },
  aboutText: {
    width: 375,
    height: 116,
    top: 75,
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 36,
    lineHeight: 42,
    color: '#003366',
  },
  iconPerson: {
    position: 'absolute',
    alignContent: 'center',
    top: 140,
    zIndex: 999,
    color: '#CACACA',
  },
  aboutCircle: {
    width: 175,
    height: 175,
    top: 30,
    borderRadius: 175/2,
    backgroundColor: '#EFEFEF',
  },
  aboutName: {
    top: 50,
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 28,
    color: '#003366',
  },
  aboutJob: {
    top: 55,
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 28,
    color: '#51B4BA',
  },
  body: {
    flex: 1,
  },
  portoBox: {
    padding: 15,
    top: 50,
    width: 300,
    height: 110,
    alignSelf: 'center',
    borderRadius: 10,
    backgroundColor: 'rgba(126, 201, 201, 0.5)'
  },
  textBoxPorto: {
    position: 'absolute',
    width: 180,
    left: 15,
    top: 5,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 17,
    color: 'white',
  },
  iconPorto: {
    padding: 5,
    margin: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  

})