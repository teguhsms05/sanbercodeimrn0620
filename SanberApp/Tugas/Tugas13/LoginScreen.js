import React, { Component } from 'react'
import { 
  View, 
  StyleSheet,
  Image,
  Text,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialIcons';



export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image source={require('./images/skillos.png')} />
          <Text style={styles.menu}>SKILLOS</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.user}>
            <Icon style={styles.iconPerson} name="person" size={25} />
            <Text style={styles.textLogin}>username / email</Text>
            <View style={styles.userWrapper}></View>
          </View>
          <View style={styles.pass}>
            <Icon style={styles.iconKey} name="vpn-key" size={25} />
            <Text style={styles.textLogin}>username / email</Text>
            <View style={styles.userWrapper}></View>
          </View>
          <Text style={styles.textAskMember}>Not a member yet?</Text>
          <View style={styles.loginBox}>
            <Text style={styles.textBoxLogin}>Login</Text>
          </View>
          <View style={styles.signUpBox}>
            <Text style={styles.textBoxSignUp}>Sign Up</Text>
          </View>
          
        </View>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D9EDEC',
  },
  logo: {
    width: 375,
    height: 116,
    top: 63,
    alignItems: 'center',
  },
  menu: {
    width: 100,
    height: 28,
    top: 20,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 24,
    lineHeight: 28,
    alignItems: 'center',
    color: '#003366',
  },
  body: {
    flex: 1,
  },
  user: {
    top: -110
  },
  iconPerson: {
    width: 24,
    height: 24,
    left: 70,
    top: 297,
  },
  userWrapper: {
    width: 200,
    height: 1,
    left: 72,
    top: 285,
    backgroundColor: 'black',
  },
  textLogin: {
    width: 125,
    height: 17,
    left: 103,
    top: 280,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 17,
  },
  pass: {
    top: -110
  },
  iconKey: {
    width: 24,
    height: 24,
    left: 70,
    top: 297,
  },
  passWrapper: {
    width: 200,
    height: 1,
    left: 72,
    top: 285,
    backgroundColor: 'black',
  },
  textPass: {
    width: 125,
    height: 17,
    left: 103,
    top: 280,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 17,
  },
  loginBox: {
    padding: 15,
    top: 210,
    width: 208,
    height: 4,
    left: 70,
    borderRadius: 10,
    backgroundColor: '#51B4BA'
  },
  textAskMember: {
    width: 208,
    height: 17,
    top: 290,
    left: 70,
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 17,
  },
  signUpBox: {
    padding: 15,
    top: 270,
    width: 208,
    height: 4,
    left: 70,
    borderRadius: 10,
    backgroundColor: '#51B4BA'
  },
  textBoxLogin: {
    position: 'absolute',
    width: 180,
    left: 10,
    top: 5,
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 17,
    color: 'white',
  },
  textBoxSignUp: {
    position: 'absolute',
    width: 180,
    left: 10,
    top: 5,
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 17,
    color: 'white',
  },
  

})