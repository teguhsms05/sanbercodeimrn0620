console.log("Tugas Conditional If-Else")
console.log('-----------------------------')
//Output untuk Input nama = 'John' dan peran = ''
var nama = ''
var peran = ''

//Output untuk Input nama = 'John' dan peran = ''
// var nama = 'Jhon'
// var peran = ''

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
// var nama = 'Jane'
// var peran = 'Penyihir'

//Output untuk Input nama = 'Jenita' dan peran 'Guard'
// var nama = 'Jenita'
// var peran = 'Guard'

//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
// var nama = 'Junaedi'
// var peran = 'Werewolf'

//"Nama harus diisi!"
if (nama == '') {
    console.log("Nama harus diisi!");
} 
//Output untuk Input nama = 'John' dan peran = ''
//"Halo John, Pilih peranmu untuk memulai game!"
else if (nama != '' && peran == ''){
    console.log("Halo %s, Pilih peranmu untuk memulai game!", nama);
}
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
// "Selamat datang di Dunia Werewolf, Jane"
// "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
else if (nama != ''){
    console.log("Selamat datang di Dunia Werewolf, %s", nama)
    if (peran == 'Penyihir'){
        console.log("Halo %s %s, kamu dapat melihat siapa yang menjadi werewolf!", peran, nama)
    } else if (peran == 'Guard'){
        console.log("Halo %s %s, kamu akan membantu melindungi temanmu dari serangan werewolf", peran, nama)
    } else if (peran == 'Werewolf'){
        console.log("Halo %s %s, kamu akan memakan mangsa setiap malam!", peran, nama)
    }
}
console.log('')
console.log("Tugas Switch Case")
console.log('-----------------------------')
var tanggal = 16;
var bulan = 6;
var tahun = 2020;
var namaBulan = '';
 
switch (bulan) {
    case 1: namaBulan = ' January ';
        break;
    case 2: namaBulan = ' February ';
        break;
    case 3: namaBulan = ' March ';
        break;
    case 4: namaBulan = ' April ';
        break;
    case 5: namaBulan = ' May ';
        break;
    case 6: namaBulan = ' June ';
        break;
    case 7: namaBulan = ' July ';
        break;
    case 8: namaBulan = ' August ';
        break;
    case 9: namaBulan = ' September ';
        break;
    case 10: namaBulan = ' October ';
        break;
    case 11: namaBulan = ' November ';
        break;
    case 12: namaBulan = ' December ';
        break;
    default:
        namaBulan = ' Undefined ';
}
console.log(tanggal + namaBulan + tahun);