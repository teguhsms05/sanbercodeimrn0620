//'LOOPING WITH WHILE'
console.log('-----------------------------')
console.log('1 LOOPING WITH WHILE')
console.log('-----------------------------')
console.log('')
console.log('LOOPING PERTAMA')
console.log('-----------------------------')
var countUp = 2
while (countUp<=20){
    console.log(countUp + ' - I love coding')
    countUp+=2
}
console.log('')
console.log('LOOPING KEDUA')
console.log('-----------------------------')
var countDown = 20
while (countDown>=2){
    console.log(countDown + ' - I will become a mobile developer')
    countDown-=2
}

// 'LOOPING WITH FOR'
console.log('')
console.log('-----------------------------')
console.log('2 LOOPING WITH FOR')
console.log('-----------------------------')
for (var count=1;count<=20;count+=1){
    if (count%2==1){
        if (count%3==0){
            console.log(count + ' - I Love Coding')
        } else {
            console.log(count + ' - Santai')
        }
    }
    else if (count%2==0){
        console.log(count + ' - Berkualitas')
    }
}

// 'MEMBUAT PERSEGI PANJANG # 8x4'
console.log('')
console.log('-----------------------------')
console.log('3 MEMBUAT PERSEGI PANJANG # 8x4')
console.log('-----------------------------')
var i = 1;
result = '';
do {
    result += Array(9).join('#')+'\n';
    i++;
}
while (i<5);
console.log(result)

// 'MEMBUAT TANGGA #'
console.log('')
console.log('-----------------------------')
console.log('4 MEMBUAT TANGGA # tinggi 7 dan alas 7')
console.log('-----------------------------')
var i = 1;
result = '';
do {
    result += Array(i).join('#')+'\n';
    i++;
}
while (i<=8);
console.log(result)

// MEMBUAT PAPAN CATUR
console.log('')
console.log('-----------------------------')
console.log('5 MEMBUAT PAPAN CATUR #')
console.log('-----------------------------')
var row = 8;
var collumn = 8;

var result = '';

for (var i = 0; i < row; i++){
    for (var j = 0; j < collumn; j++){
        if ((i+j)%2 == 0){
            result += " ";
        } else {
            result += "#";
        }
    }
    result += "\n";
}

console.log(result);
