console.log('-----------------------------')
console.log('NO 1')
console.log('-----------------------------')
function teriak() {
    return "Halo Sanbers!"
    
}
console.log(teriak()) // "Halo Sanbers!" 

console.log('')
console.log('-----------------------------')
console.log('NO 2')
console.log('-----------------------------')
function kalikan(one,two) {
    return (one*two)    
}

var num1 = 12
var num2 = 4


var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 30 

console.log('')
console.log('-----------------------------')
console.log('NO 3')
console.log('-----------------------------')
function introduce(name, age, address, hobby) {
    let result = (`Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`)
    return result
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamacleart saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"