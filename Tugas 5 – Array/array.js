// 1 RANGE
console.log('')
console.log('-----------------------------')
console.log('1 RANGE')
console.log('-----------------------------')
function range(startNum, finishNum) {
    var result = [];
    if (startNum < finishNum){
        for (i = startNum; i <= finishNum; i++){
            result.push(i);
        }
        if (result.length == 0){
            return -1;
        } else {
            return result;
        }
    } else {
        for (i = startNum; i >= finishNum; i--){
            result.push(i);
        }
        if (result.length == 0){
            return -1;
        } else {
            return result;
        }

    }
    
    
} 
// Code di sini
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// 2 RANGE WITH STEP
console.log('')
console.log('-----------------------------')
console.log('2 RANGE WITH STEP')
console.log('-----------------------------')
function rangeWithStep(startNum, finishNum, step) {
    var result = [];
    if (startNum < finishNum){
        for (i = startNum; i <= finishNum; i+=step){
            result.push(i);
        }
        if (result.length == 0){
            return -1;
        } else {
            return result;
        }
    } else {
        for (i = startNum; i >= finishNum; i-=step){
            result.push(i);
        }
        if (result.length == 0){
            return -1;
        } else {
            return result;
        }

    }
}

// Code di sini
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// 3 SUM OF RANGE
console.log('')
console.log('-----------------------------')
console.log('3 SUM OF RANGE')
console.log('-----------------------------')
function sum(startNum, finishNum, step) {
    let result = [];
    let reducer = (a, b) => a + b;
    if (step == null){
        step = 1;
    } if (finishNum == null || step == null){
        finishNum = step = 1;
    } if (startNum == null){
        return 0;
    }
    if (startNum < finishNum){
        for (i = startNum; i <= finishNum; i+=step){
            result.push(i);
        }
    } else {
        for (i = startNum; i >= finishNum; i-=step){
            result.push(i);
        }
    }
    lastResult = result.reduce(reducer);
    return lastResult
}

// Code di sini
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// 4 Array Multidimensi
console.log('')
console.log('-----------------------------')
console.log('4 Array Multidimensi')
console.log('-----------------------------')
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(data){
    for (let i=0; i<data.length;i++){
        
        var inArray = data[i].length;
        for (let j = 0; j < inArray; j++) {
            if (j == 0){
                console.log('Nomor ID: '+ data[i][j]);
            } if (j == 1){
                console.log('Nama Lengkap: '+ data[i][j]);
            } if (j == 2 ){
                console.log('TTL: '+ data[i][j] + ' ' + data[i][j+1]);
            } if (j == 4){
                console.log('Hobi: '+ data[i][j] );
                console.log('\n');
            }
        }
        
    }
    // return result
}
return dataHandling(input)

// 5 Balik Kata
console.log('')
console.log('-----------------------------')
console.log('5 Balik Kata')
console.log('-----------------------------')
function  balikKata(kata){
    var result = kata.split('').reverse().join('');
    return result
}
console.log(balikKata("Kasur Rusak"));// kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 

// 6 Metode Array
console.log('')
console.log('-----------------------------')
console.log('6 Metode Array')
console.log('-----------------------------')
function  dataHandling2(data){
    for (i=0; i<=data.length;i++){
        if (i==1){
            data.splice(i,0,'Roman Alamsyah Elsharawy');
        } if (i==2){
            data.splice(i,i,'Provinsi Bandar Lampung');
        } if (i==4){
            data.splice(i,1);
            data.splice(i,0,'Pria');
            data.splice(i+1,0,'SMA Internasional Metro');
        } 
    }
    var bulan = Number(data[3].split('/')[1]);
    switch (bulan) {
        case 01: bulan = ' Januari ';
            break;
        case 02: bulan = ' Februari ';
            break;
        case 03: bulan = ' Maret ';
            break;
        case 04: bulan = ' April ';
            break;
        case 05: bulan = ' Mei ';
            break;
        case 06: bulan = ' Juni ';
            break;
        case 07: bulan = ' Juli ';
            break;
        case 08: bulan = ' Agustus ';
            break;
        case 09: bulan = ' September ';
            break;
        case 10: bulan = ' Oktober ';
            break;
        case 11: bulan = ' November ';
            break;
        case 12: bulan = ' Desember ';
            break;
        default:
            bulan = ' Undefined ';
    }
    var date = data[3].split('/');
    date.sort(function (value1, value2) { return value2 - value1 } ) ;
    var date2 = data[3].split('/').join('-');
    var name = data[1].slice(0,15);
    // print output
    console.log(data);
    console.log(bulan);
    console.log(date);
    console.log(date2);
    console.log(name);
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);