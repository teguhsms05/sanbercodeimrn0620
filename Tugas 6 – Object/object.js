// 1 ARRAY TO OBJECT
console.log('')
console.log('-----------------------------')
console.log('1 ARRAY TO OBJECT')
console.log('-----------------------------')

function arrayToObject(arr) {
    //Jika tahun tidak terdefinisi atau tahunnya lebih besar dibandingkan dengan tahun sekarang maka kembalikan nilai : “Invalid birth year”.
    // Code di sini 
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    var result = []
    if (arr==''){
        return ''
    }
    for (i=0;i<arr.length;i++){
        let birthYear = ''
        if (arr[i][3] > thisYear || arr[i][3] == null){
            birthYear = "Invalid birth year"
        } else {
            birthYear = thisYear-arr[i][3];
        }
        
        result.push({firstName: arr[i][0], lastName: arr[i][1], gender: arr[i][2], age: birthYear });
    }
    // console.log(`${result.firstName} ${result.lastName} :`, result)
    for (j=0;j<result.length;j++){
        console.log(j+1 + '. ' + result[j].firstName + ' ' + result[j].lastName + ': ', result[j]);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
// /*
//     1. Tony Stark: { 
//         firstName: "Tony",
//         lastName: "Stark",
//         gender: "male",
//         age: 40
//     }
//     2. Pepper Pots: { 
//         firstName: "Pepper",
//         lastName: "Pots",
//         gender: "female".
//         age: "Invalid Birth Year"
//     }
// */
 
// // Error case 
arrayToObject([]) // ""

// 2 Shopping Time
console.log('')
console.log('-----------------------------')
console.log('2 Shopping Time')
console.log('-----------------------------')
function shoppingTime(memberId, money) {
    // you can only write your code here!
    var sepatu = [{item: 'Sepatu Stacattu', harga: 1500000}]
    var baju = [{item: 'Baju Zoro', harga: 500000},{item: 'Baju H&N', harga: 250000}]
    var sweater = [{item: 'Sweater Uniklooh', harga: 175000}]
    var casing = [{item: 'Casing Handphone', harga: 50000}]
    var listItem = [
        {item: 'Sepatu Stacattu', harga: 1500000},
        {item: 'Baju Zoro', harga: 500000}, 
        {item: 'Baju H&N', harga: 250000},
        {item: 'Sweater Uniklooh', harga: 175000},
        {item: 'Casing Handphone', harga: 50000}
    ]
    var result = {}
    var listPurchased = []
    let moneySpend = money;
    if (memberId == '' || memberId == null){
        return ('Mohon maaf, toko X hanya berlaku untuk member saja')
    } else if (money < 50000){
        return ('Mohon maaf, uang tidak cukup')
    } else {
        result.memberId = memberId
        result.money = money
        for (i=0;i<listItem.length;i++){
            if (moneySpend >= listItem[i].harga){
                listPurchased.push(listItem[i].item)
                moneySpend = moneySpend-listItem[i].harga
            } else if (moneySpend >= listItem[i].harga){
                listPurchased.push(listItem[i].item)
                moneySpend = moneySpend-listItem[i].harga
            } else if (moneySpend >= listItem[i].harga){
                listPurchased.push(listItem[i].item)
                moneySpend = moneySpend-listItem[i].harga
            } else if (moneySpend >= listItem[i].harga){
                listPurchased.push(listItem[i].item)
                moneySpend = moneySpend-listItem[i].harga
            } else if (moneySpend >= listItem[i].harga){
                listPurchased.push(listItem[i].item)
                moneySpend = moneySpend-listItem[i].harga
            }
            
            
        }
        result.listPurchased = listPurchased
        result.changeMoney = moneySpend
        
    }
    return result
    //return (Object.keys(listPurchased))

  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// 3 Naik Angkot
console.log('')
console.log('-----------------------------')
console.log('3 Naik Angkot')
console.log('-----------------------------')
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    // console.log(rute.length) //6
    //console.log(rute.indexOf('C'));
    // console.log(arrPenumpang.length) //2, 0
    result = []
    for (i=0;i<arrPenumpang.length;i++){
        result.push({
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: (rute.indexOf(arrPenumpang[i][2])-rute.indexOf(arrPenumpang[i][1]))*2000
        })
    }
    return result
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]