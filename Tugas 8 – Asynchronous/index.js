// di index.js
var readBooks = require('./callback.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var time = 10000;

function result(i,n){
    if (i < books.length){
        return readBooks(time, books[i], function(){
            time -= books[i].timeSpent
            result(i+1,n-1)
        } )   
    } 
}
result(0,books.length)

