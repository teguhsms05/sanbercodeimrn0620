var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
var time = 10000
function result(i,n){
    if (i < books.length){
        return readBooksPromise(time,books[i])
        .then(function (fullRead){
            console.log(fullRead);
            time -= books[i].timeSpent
            result(i+1,n-1)
        })
        .catch(function (error){
            console.log('error')
            console.log(error.message);
        });
    } 
}
result(0,books.length)