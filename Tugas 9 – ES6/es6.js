// 1 Mengubah fungsi menjadi fungsi arrow
console.log('')
console.log('-----------------------------')
console.log('1 Mengubah fungsi menjadi fungsi arrow')
console.log('-----------------------------')
const golden = () => {
    console.log("this is golden!!")
}
golden()

// 2 Sederhanakan menjadi Object literal di ES6
console.log('')
console.log('-----------------------------')
console.log('2 Sederhanakan menjadi Object literal di ES6')
console.log('-----------------------------')
const newFunction = (firstName, lastName) => {
    return ({
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
            console.log(`${firstName}` + " " + `${lastName}`)
            return
        }
    }) 
}  
//Driver Code 
newFunction("William", "Imoh").fullName() 

// 4 Array Spreading
console.log('')
console.log('-----------------------------')
console.log('4 Array Spreading')
console.log('-----------------------------')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// 5 Template Literals
console.log('')
console.log('-----------------------------')
console.log('5 Template Literals')
console.log('-----------------------------')
const planet = "earth"
const view = "glass"

var before = 'Lorem ' + `${view}` + 'dolor sit amet, ' +  
'consectetur adipiscing elit,' + `${planet}` + 'do eiusmod tempor ' +
'incididunt ut labore et dolore magna aliqua. Ut enim' +
' ad minim veniam'
 
// Driver Code
console.log(before) 